# Skyking General Materials
Some materials, guides, and resources for Skyking threads on 4chan.

# Contents
* [OP pasta](op.txt) - OP pasta for Skyking threads
* [OP image](op.png) - The usual OP image
* [Alternative OP image](misc-photo.jpg) 
* [Guide](newfag-guide.png) - Set up guide for new friends
* [US Airforce CSV file](usairforce-memories.csv) - CSV file containing US Air Force frequencies. It can be imported on the memories tab on http://websdr.ewi.utwente.nl:8901/
* [Understanding Skyking Messages](high-priority-messages.png) - Guide to understanding skyking and high priority messages
* [Anon's Shopping List](anons-shopping-list.png) - Babby's first ham
